# Docker

## Terminal

* ^C : permet d'interompre le processus.

```terminal
docker-compose up
```

```terminal
docker kill $(docker ps -q)
```

Tue le processus invisible en cours.

## Configuration

```yml
version: "3"
services:
  nginx:
   image: library/nginx:latest
   volumes:
    - ".:/usr/share/nginx/html"
   ports:
    - "80:80"
   networks:
    - nginx
networks:
  nginx:

volumes:
  socket:
```

> Exemple de fichier de configuration : docker-compose.yml