# git : gestionnaire de versionning

## Commandes

Git est un gestionnaire de versioning permettant de :

* partager du code
* travailler collaborativement
* avoir un historique des modifications
* avoir des dépôts disant sur des serveurs.

***

> Il en existe d'autres comme [svn](https://fr.wikipedia.org/wiki/Apache_Subversion) et [mercurial](https://www.mercurial-scm.org/).

***

* GitHub est une plateforme qui offre un service git.
* GitLab est un logiciel libre que l'on peut installer chez soi, et c'est aussi une plateforme à l'instar de GitHub.

Ils permettent de gérer les projets de manière plus simple.

![Schema : staging -> dépot](https://cdn-images-1.medium.com/max/1600/0*AtDEJJwMtdcMMVrQ.png)

(Il faudrait trouver une image libre, avec ses sources)

```terminal
git init
```

Initialise le dossier en dépot git local.

```terminal
git config --global user.email "mail@domaine.com"
git config --global user.name "name"
```

Configuration indispensable pour utiliser git en ligne de commande. À renseigner à l'identique de la plateforme GitLab pour pouvoir pusher directement sur le serveur.

```terminal
git clone monAdresse
```

Permet de cloner un dépot distant depuis la plateforme git.

```terminal
git status
```

Renseigne le statut du dépot, à savoir les modifications en attente de validation dans le working directory.

```terminal
git log
```

Affiche les commits.

```terminal
git branch maBranche
```

Crée une nouvelle branche appellé maBranche. Ou l'écrase si elle existait déja.

```terminal
git checkout
```

re-initialise le fichier tel qu'il était dans le dépôt.

```terminal
git checkout maBranche
```

Permet de changer de branche.

```terminal
git add
```

Ajoute la modification au staging area.

```terminal
git commit -m "message de commit"
```

Ajoute les modifications au dépot.

```terminal
git pull maRemote maBranche
```

Télécharge les modifications depuis mon dépos distant sur ma branche.

```terminal
git push maRemote maBranche
```

Publie les modifications locales sur le dépot distant.

```terminal
git merge
```

Fusionne la branche appellé avec celle sur laquelle on se trouve.

```terminal
git remote add maNouvelleRemote monDépotDistant
```

Donne un surnom à mon dépot distant !

## Méthodologie

## Liens utiles

* [Wikipédia](https://fr.wikipedia.org/wiki/Git)
* [Cheat-sheet](https://www.git-tower.com/blog/git-cheat-sheet)